/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

// Using promises and the `fetch` library, do the following. 
// If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.
// Usage of the path libary is recommended





// 1. Fetch all the users

let users = fetch("https://jsonplaceholder.typicode.com/users")

users
  .then((res) => {
    if (res.ok) {
      return res.json()
    }
    else {
      console.log("Data not found")
    }
  })
  .then((data) => {
    if (data != undefined) {
      console.log(data)
    }
  })
  .catch((err) => {
    console.log(err)
  })


// 2. Fetch all the todos

let todos = fetch("https://jsonplaceholder.typicode.com/todos")

todos
  .then((res) => {
    if (res.ok) {
      return res.json()
    }
    else {
      console.log("Data not found")
    }
  })
  .then((data) => {
    if (data != undefined) {
      console.log(data)
    }
  })
  .catch((err) => {
    console.log(err)
  })




// 3. Use the promise chain and fetch the users first and then the todos.

users = fetch("https://jsonplaceholder.typicode.com/users")
todos = fetch("https://jsonplaceholder.typicode.com/todos")

users
  .then((res) => {
    if (res.ok) {
      return res.json()
    }
    else {
      console.log("Data not found")
    }
  })
  .then((data) => {
    if (data != undefined) {
      console.log(data)
    }
    return todos
  })
  .then((res) => {
    if (res.ok) {
      return res.json()
    }
    else {
      console.log("Data not found")
    }
  })
  .then((data) => {
    if (data != undefined) {
      console.log(data)
    }
  })
  .catch((err) => {
    console.log(err)
  })




// 4. Use the promise chain and fetch the users first and then all the details for each user.

// let users = fetch("https://jsonplaceholder.typicode.com/users")

// users
//   .then((res) => {
//     if (res.ok) {
//       return res.json()
//     }
//     else {
//       console.log("Data not found")
//     }
//   })
//   .then((data) => {
//     if (data != undefined) {
//       console.log(data)
//     }
//   })
//   .catch((err) => {
//     console.log(err)
//   })



// // 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo




